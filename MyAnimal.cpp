#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "...\n";
	};
};

class Dog: public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!\n";
	}
};

class Duck : public Animal
{
public:
	void Voice() override 
	{
		std::cout << "Quack!\n";
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Moo!\n";
	}
};



int main()
{
	const int length = 10;
	Animal* array[length] = {new Dog(), new Cow(), new Duck(), new Duck(), new Cat(), new Cow(), new Duck(), new Cow(), new Cat(), new Cat()};
	for (int i = 0; i < length; i++)
	{
		array[i]->Voice();
	}
}